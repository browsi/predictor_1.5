import hashlib
import os
import shutil

import pandas as pd
import tensorflow as tf
from AFM_CatBoost.configuration import savedModelPathAFM, savedModelPathCatboost
from tensorflow.python.keras import backend as K

def fill_analytics(data, features):
    """ This function fills the missing values in the user_analytics features """
    print('Building Dict')
    aggregation=dict((col, 'mean') for col in features['user_analytics']) #--> {'col1': 'mean', 'col2': 'mean' ..}
    df_grouped=data.groupby('article_id').agg(aggregation) # mean value of each feature per article id
    df_grouped.fillna(df_grouped.mean(),inplace=True)
    mapping_dict=df_grouped.to_dict(orient='index') #--> {'Article 1' : {'feature1': value, 'feature2':value ...}, 'Article 2': {'feature1': value ...}}
    for col in features['user_analytics']: # fill the missing values in the dataframe with values from dict
        print('Filling {}'.format(col))
        data[col]=data.apply(lambda x: mapping_dict[x['article_id']][col] if pd.isnull(x[col]) else x[col],axis=1)
    return data


def preprocess(data, features):
    """ This function fills NAs, encode the nominal features and cap the target """
    cols = []
    target = features['target']
    # mms = MinMaxScaler(feature_range=(0, 1))
    for col in data.columns:
        if col in features['numeric']:  # fill na for numeric
            print('Preprocessing Numeric column {}'.format(col))
            data[col].fillna(data[col].mean(), inplace=True)
            cols.append(col)
        elif col in features['nominal'].keys():  # fill na and encode nominal
            print('Preprocessing Nominal column {}'.format(col))
            data[col].fillna("NA_{}".format(col), inplace=True)
            data[col] = data[col].apply(
                lambda x: int(hashlib.sha1(x.encode('utf-8')).hexdigest(), 16) % (features['nominal'][col]))

            cols.append(col)
        else:
            continue
    print('Preprocessing Analytics features')
    data = fill_analytics(data, features)  # fill na for special case --> user analytics features
    # data[features['numeric']+features['user_analytics']]=mms.fit_transform(data[features['numeric']+features['user_analytics']])
    cols = cols + features['user_analytics']
    return data[cols], data[target]


def saveModelAFM(model):

    dirpath=savedModelPathAFM
    if os.path.exists(dirpath) and os.path.isdir(dirpath):
        shutil.rmtree(dirpath)

    signature = tf.saved_model.signature_def_utils.predict_signature_def(
        inputs={'input': tf.convert_to_tensor(model.input)}, outputs={'probability': tf.convert_to_tensor(model.output)})

    builder = tf.saved_model.builder.SavedModelBuilder(dirpath)
    builder.add_meta_graph_and_variables(
        sess=K.get_session(),
        tags=[tf.saved_model.tag_constants.SERVING],
        signature_def_map={
            tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:
                signature
        })
    builder.save()
    print('AFM model saved')

def saveModelCatboost(model, name='catboost_1.5'):
    model.save_model(savedModelPathCatboost+"{}.bin".format(name))
    print('Catboost model saved')


