trainPath='./data/pred_1.5_take4_train.csv'
testPath='./data/pred_1.5_take4_test.csv'
features={'numeric':  ['fold', 'attempt_index',  'screen_length', 'viewability_per_article_and_ad_index',
                       'is_refreshed', 'is_lazy_loaded', 'is_saas',
                      'is_dcs'],
         'nominal': {'article_id': 10**4, 'user_id': 2*10**4, 'geo_location': 10**2, 'position_type': 3,
                     'device': 10**2, 'browser': 10**1,'browser_type':2, 'traffic_source':3,'provider': 3, 'query_params': 10**2},
         'user_analytics':['user_velocity_avg', 'user_velocity_static', 'pav', 'bav','avg_user_latency'],
         'target': 'is_viewed'}
sparse_features=[ 'article_id', 'query_params',
       'geo_location', 'traffic_source', 'user_id', 'position_type']
dense_features=[ 'is_dcs', 'is_saas', 'is_refreshed', 'is_lazy_loaded']
AFMconfig={'l2_reg_linear':0.00002, 'l2_reg_embedding':0.00005, 'batch_size':25600, 'epochs': 30, 'validation_split': 0.2}
catboostConfig={'one_hot_max_size': 5, 'depth': 5, 'iterations': 50, 'l2_leaf_reg': 0.01, 'learning_rate':0.15}
savedModelPathAFM='./saved_models/AFM'
savedModelPathCatboost='./saved_models/Catboost'
saveCode=3 #posssible options 0: no model, 1: AFM, 2:Catboost, 3: Both
