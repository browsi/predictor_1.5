from collections import OrderedDict, namedtuple
from itertools import chain
from tensorflow.python.keras.initializers import RandomNormal
from tensorflow.python.keras.layers import (Concatenate, Dense, Embedding,Input,  add)
from tensorflow.python.keras.regularizers import l2

SingleFeat = namedtuple('SingleFeat', ['name', 'dimension', ])

def check_feature_config_dict(feature_dim_dict):
    if not isinstance(feature_dim_dict, dict):
        raise ValueError(
            "feature_dim_dict must be a dict like {'sparse':{'field_1':4,'field_2':3,'field_3':2},'dense':['field_4','field_5']}")
    if "sparse" not in feature_dim_dict:
        feature_dim_dict['sparse'] = []
    if "dense" not in feature_dim_dict:
        feature_dim_dict['dense'] = []
    if not isinstance(feature_dim_dict["sparse"], list):
        raise ValueError("feature_dim_dict['sparse'] must be a list,cur is", type(
            feature_dim_dict['sparse']))

    if not isinstance(feature_dim_dict["dense"], list):
        raise ValueError("feature_dim_dict['dense'] must be a list,cur is", type(
            feature_dim_dict['dense']))


def create_singlefeat_dict(feature_dim_dict, prefix=''):
    """ Iterates feature_dim_dict and creates input layers and puts them into two dicts (dense and sparse)
    -->  {'browser': Input(None,1), 'device': Input(None, 1)}
    -->  {'fold': Input(None, 1)}}"""
    sparse_input = OrderedDict()
    for i, feat in enumerate(feature_dim_dict["sparse"]):
        sparse_input[feat.name] = Input(
            shape=(1,), name=prefix+'sparse_' + str(i) + '-' + feat.name)

    dense_input = OrderedDict()

    for i, feat in enumerate(feature_dim_dict["dense"]):
        dense_input[feat] = Input(
            shape=(1,), name=prefix+'dense_' + str(i) + '-' + feat.name)
    return sparse_input, dense_input



def create_embedding_dict(feature_dim_dict, embedding_size, init_std, seed, l2_reg, prefix='sparse', seq_mask_zero=True):

    """Returns an Embedding dict for sparse features---> {'device': Embedding(None, 1, embedding size) ... }"""
    sparse_embedding = {feat.name: Embedding(feat.dimension, embedding_size,
                                                 embeddings_initializer=RandomNormal(
                                                 mean=0.0, stddev=init_std, seed=seed),
                                                 embeddings_regularizer=l2(l2_reg),
                                                 name=prefix+'_emb_' + str(i) + '-' + feat.name)
                        for i, feat in enumerate(feature_dim_dict["sparse"])}
    return sparse_embedding



def get_inputs_embedding(feature_dim_dict, embedding_size, l2_reg_embedding, l2_reg_linear, init_std, seed,
                         sparse_input_dict, dense_input_dict, include_linear):

   # Get the embedding layeres for sparse features and put in dict
    deep_sparse_emb_dict = create_embedding_dict(feature_dim_dict, embedding_size, init_std, seed, l2_reg_embedding)

   # Extarct to list
    deep_emb_list = get_embedding_vec_list(deep_sparse_emb_dict, sparse_input_dict)

    # Additional Linear Embeddings (embedding size=1) for sparse features
    if include_linear:
        linear_sparse_emb_dict = create_embedding_dict(feature_dim_dict, 1, init_std, seed, l2_reg_linear, 'linear')
        # Extarct to list
        linear_emb_list = get_embedding_vec_list(linear_sparse_emb_dict, sparse_input_dict)
    else:
        linear_emb_list = None

    inputs_list = get_inputs_list([sparse_input_dict, dense_input_dict])     # Concatenate all Input(None,1) columns to list
    return inputs_list, deep_emb_list, linear_emb_list


def get_embedding_vec_list(embedding_dict, input_dict):
    return [embedding_dict[feat](v) for feat, v in input_dict.items()]


def get_inputs_list(inputs):
    return list(chain(*list(map(lambda x: x.values(), filter(lambda x: x is not None, inputs)))))

def get_linear_logit(linear_emb_list, dense_input_dict, l2_reg):
    if len(linear_emb_list) > 1:
        linear_term = add(linear_emb_list)
    elif len(linear_emb_list) == 1:
        linear_term = linear_emb_list[0]
    else:
        linear_term = None
    dense_input = list(dense_input_dict.values())
    if len(dense_input) > 0:
        dense_input__ = dense_input[0] if len(dense_input) == 1 else Concatenate()(dense_input)
        linear_dense_logit = Dense(1, activation=None, use_bias=False, kernel_regularizer=l2(l2_reg))(dense_input__)
        if linear_term is not None:
            linear_term = add([linear_dense_logit, linear_term])
        else:
            linear_term = linear_dense_logit
    return linear_term

def preprocess_input_embedding(feature_dim_dict, embedding_size, l2_reg_embedding, l2_reg_linear, init_std, seed, return_linear_logit=True):

    # Creates two dicts of Input() layers for sparse and dense features
    sparse_input_dict, dense_input_dict = create_singlefeat_dict(feature_dim_dict)

    # Creates three lists:
    # 1. input list [Input(None,1), Input(None,1) ...]
    # 2. embedding list [Embedding( None ,1 , embedding_size) ... ]
    # 3. linear embedding list [Embedding (None ,1 , 1) ...]
    inputs_list, deep_emb_list, linear_emb_list = get_inputs_embedding(feature_dim_dict, embedding_size,
                                                                       l2_reg_embedding, l2_reg_linear, init_std, seed,
                                                                       sparse_input_dict, dense_input_dict,
                                                                        return_linear_logit)
    if return_linear_logit:
        linear_logit = get_linear_logit(linear_emb_list, dense_input_dict, l2_reg_linear)
    else:
        linear_logit = None
    return deep_emb_list, linear_logit, inputs_list

