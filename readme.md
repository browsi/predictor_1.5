# Predictor 1.5

This is the new viewability predictor 1.5 for mobile. The new version adds additional user features, and a more sophisticated architecture.
The output of the model is the probability of a single spot in a specific page_view to be viewed

## Architecture
In order to outperform it's predecessor, Predictor 1.5 uses two models
1. AFM (Attentional Factorization Machine), based on this paper: https://www.comp.nus.edu.sg/~xiangnan/papers/ijcai17-afm.pdf
2. Categorial Boost (aka Catboost)

features---> AFM --> AFM output (a number between 0 and 1) + more features ---> Catboost --> prediction (a number between 0 and 1)

### AFM
Factorization machines (aka FM) are logistic/linear regressors that add a second-order feature interactions as well. FM use latent-feature embeddings to deal with very sparse data and compute the result in linear time complexity.
On top of this an Attention mechanism is added which adds weights according to the importance of different feature interactions, hence it is called Attentional Factorization Machine.
![Alt text](./attention.png?raw=true "AFM architecture")

###Catboost
It is an open source library that implements gradient boosting decision trees. It differs from XGboost in the way it deals with categorical variables.
It can deal with very high cardinality features as well as unseen values, using a very smart mapping. It is developed by Yandex. https://tech.yandex.com/catboost/

## Getting Started

At the moment the data is fetched manually via the ./SQL/predictor_1.5_query.sql script. Run it twice, once for train data and once for test data (make sure to change the dates in the query)

### Prerequisites

Python 3.5
hashlib, collections, itertools, shutil, os, pandas, sklearn (all native to python)
tesorflow==1.12.0
catboost==0.13

```
pip3 install tensorflow==1.12.0
```

### Installing

There are two executables:
1. train_model.py (run this to train model, configurations should be tweaked in ./AFM_Catboost/configuraion.py) . Models will be saved to local fs.
2. Pred_1.5_cat+AFM.ipynb (run this Jupyter for research and evaluation purpose)


```
./python3 train_model.py
```

## Features of the model

As described above each model (AFM, Catboost) gets different features

### Features AFM
1.  'article_id'
2.  'query_params'
3.  'geo_location'
4.  'traffic_source'
5.  'user_id'
6.  'position_type'
7.  'is_dcs'
8.  'is_saas'
9.  'is_refreshed'
10. 'is_lazy_loaded'

Features 1-6  are nominal features.
Features 7-10 are boolean features.
Each nominal feature should be encoded from a string to a number (*** Not a One-Hot vector ***)
The encoding should be used with the built-in python module 'hashlib'
The hashing function remains consistent across all python sessions, ensuring that we get the same encoding each time
The example code maps the user_id column from a string column to a numeric column with values between 0-10000 (the hash size per column could be found in 'configurations.py'):

```
import hashlib
data['user_id'] = data[user_id].apply(
                lambda x: int(hashlib.sha1(x.encode('utf-8')).hexdigest(), 16) % (10**5)
```
After applying this hashing function on the six nominal columns (of strings) we get six columns of ints (the boolean columns do not need any preprocessing)
The order of the columns must be kept


### Features Catboost
1. 'article_id'
2. 'user_id'
3. 'device'
4. 'traffic_source'
5. 'browser_type'
6. 'geo_location'
7. 'query_params'
8. 'provider'
9. 'position_type'
10. 'browser',
11. 'fold'
12. 'attempt_index'
13. 'screen_length'
14. 'viewability_per_article_and_ad_index'
15. 'is_refreshed'
16. 'is_lazy_loaded'
17. 'is_saas'
18. 'is_dcs'
19. 'user_velocity_avg'
20. 'user_velocity_static'
21. 'pav'
22. 'bav'
23. 'avg_user_latency'
24. 'AFM'

Features 1-10 are nominal features and they should be encoded exactly as noted above.
Features 11-13 are numeric features and do not require any preprocessing or Optimzer calculations.
Feature 14 is calculate in the Optimzer (exactly the same as Predictor 1.4)
Features 15-18 are boolean features and do not require any preprocessing or Optimzer calculations.
Features 19-23 come from the user_analytics stream. The Optimizer should hold a mapping of Article_id--> means of columns 19-23
For example
```
{'article_id': "TKXMS85F", 'analyitcs_means': {'mean_user_velocity_avg': 12.5 ,'mean_user_velocity_static': 0.07 , 'mean_pav': 0.87, 'mean_bav': 0.74, 'mean_avg_user_latency': 5230}}
```
Every time one of Feature 19-23 is null it should be replaced by the corresponding value from the mapping.

Feature 24 is the output of the AFM model (number between 0 and 1)



