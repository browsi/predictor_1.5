from AFM_CatBoost.AFM_logic import AFM
from AFM_CatBoost.AFM_preprocess import SingleFeat
from AFM_CatBoost.configuration import trainPath, testPath, features, sparse_features, dense_features, AFMconfig, catboostConfig, saveCode, savedModelPathAFM, savedModelPathCatboost
from AFM_CatBoost.utils import preprocess,  saveModelAFM ,saveModelCatboost
import pandas as pd
import catboost

if __name__ == "__main__":
    print('\x1b[6;30;42m' + 'Make Sure to change SaveModel in config file!' + '\x1b[0m')
    assert saveCode in (0,1,2,3)
    data_train = pd.read_csv(trainPath, nrows=100)
    data_test = pd.read_csv(testPath, nrows=100)
    X_train,y_train=preprocess(data_train,features)
    X_test,y_test=preprocess(data_test,features)
    sparse_feature_list = [SingleFeat(feat, features['nominal'][feat]) for feat in sparse_features]
    dense_feature_list = [SingleFeat(feat, 0) for feat in dense_features]
    afm = AFM(feature_dim_dict={"sparse": sparse_feature_list,"dense": dense_feature_list},
               l2_reg_linear=AFMconfig['l2_reg_linear'], l2_reg_embedding=AFMconfig['l2_reg_embedding'],  final_activation='sigmoid')
    afm.compile( optimizer='Adam', loss="binary_crossentropy", metrics=['accuracy'])
    train_model_input = [X_train[feat.name].values for feat in sparse_feature_list] + \
                        [X_train[feat.name].values for feat in dense_feature_list]
    test_model_input = [X_test[feat.name].values for feat in sparse_feature_list] + \
                       [X_test[feat.name].values for feat in dense_feature_list]
    afm.fit(train_model_input, y_train.values, batch_size=AFMconfig['batch_size'], epochs=AFMconfig['epochs'], validation_split=AFMconfig['validation_split'])
    print('AFM model trained, columns order is')
    for feat in sparse_feature_list+dense_feature_list:
        print(feat.name)
    y_hat_AM_train = afm.predict(train_model_input)
    y_hat_AM_test = afm.predict(test_model_input)
    X_train_catboost = X_train[list(features['nominal'].keys()) + features['numeric'] + features['user_analytics']]
    X_test_catboost = X_test[list(features['nominal'].keys()) + features['numeric'] + features['user_analytics']]
    X_train_catboost = X_train_catboost.assign(AFM=y_hat_AM_train.reshape(-1))
    X_test_catboost = X_test_catboost.assign(AFM=y_hat_AM_test.reshape(-1))
    cb = catboost.CatBoostClassifier(eval_metric="Logloss", one_hot_max_size=catboostConfig['one_hot_max_size'], depth=catboostConfig['depth'],
                                     iterations=catboostConfig['iterations'], l2_leaf_reg=catboostConfig['l2_leaf_reg'],
                                     learning_rate=catboostConfig['learning_rate'])
    cb.fit(X_train_catboost, y_train, cat_features=list(range(len(features['nominal'].keys()))))
    print('Catboost model trained, columns order is')
    print(X_train_catboost.columns)
    if saveCode in (1,3):
        saveModelAFM(afm)
        print('AFM model saved to {}'.format(savedModelPathAFM))
    if saveCode in (2,3):
        saveModelCatboost(cb)
        print('CatBoost model save to {}'.format(savedModelPathCatboost))
    if saveCode==0:
        print('\x1b[6;30;42m' + 'No model saved' + '\x1b[0m')




