select  S.article_id, S.user_id, query_params, geo_location, traffic_source, device, browser, browser_type, screen_length,
is_refreshed, is_saas, is_lazy_loaded, is_dcs,  provider, fold, attempt_index, position_type,browsi_avg_time_in_view,
       user_velocity_avg, user_velocity_static, pav, bav, avg_user_latency, viewability_per_article_and_ad_index, is_viewed


from
(
select A.article_id, A.user_id, A.page_view_id,    query_params, geo_location, traffic_source, device, browser, browser_type, screen_length
  from
(
select article_id, user_id, page_view_id,  query_params
from supply_parquet
where day>=26 and month=3 AND device_type<>'DESKTOP' and event_type='supply_config_sent'  and site_key<>'ynetapp'
) A

inner join

(
select page_view_id, geo_location, traffic_source, device, browser, browser_type, screen_length
from supply_parquet
where day>=26 and month=3 AND device_type<>'DESKTOP' and event_type='page_view' and is_benchmark_pageview=false  and site_key<>'ynetapp'
) B

on A.page_view_id=B.page_view_id



) S


inner join

(
select  article_id, A.page_view_id, A.ad_index,
case when A.refresh_count>0 then 1 else 0 end as is_refreshed,
case when is_saas=true then 1 else 0 end as is_saas,
case when is_lazy_loaded=true then 1 else 0 end as is_lazy_loaded,
case when is_dcs=true then 1 else 0 end as is_dcs,
provider, fold, attempt_index, position_type,
case when B.page_view_id is null then 0 else 1 end as is_viewed
from

(
select  article_id, page_view_id, ad_index, refresh_count, provider, fold, attempt_index, is_saas, is_lazy_loaded, is_dcs, position_type
from demand_parquet
where day>=26 and month=3 AND device_type<>'DESKTOP' and event_type='ad_impression' and site_key<>'ynetapp'
) A


left outer join


(
select page_view_id, ad_index, refresh_count
from demand_parquet
where day>=26 and month=3 AND device_type<>'DESKTOP' and event_type='viewed_impression' and counter=1 and site_key<>'ynetapp'
) B

on A.page_view_id=B.page_view_id and A.ad_index=B.ad_index and A.refresh_count=B.refresh_count

) D

on S.page_view_id=D.page_view_id

left outer join

(
select article_id, ad_index, sum(v)/cast(sum(i) as double) as viewability_per_article_and_ad_index
from
(
select article_id, page_view_id, ad_index, refresh_count,
count(case when event_type='ad_impression' then 1 end) as i,
count(case when event_type='viewed_impression' and counter=1 then 1 end) as v
from demand_parquet
where day>=26 and month=3 AND device_type<>'DESKTOP' and event_type in ('viewed_impression', 'ad_impression')   and site_key<>'ynetapp'
group by 1,2,3,4
) AA
group by 1,2
having sum(i)>=10
) V

on D.article_id=V.article_id and D.ad_index=V.ad_index


left outer join

(

select user_id, browsi_avg_time_in_view, user_velocity_avg, user_velocity_static, pav,bav,avg_user_latency
from
(
select user_id, browsi_avg_time_in_view,
       user_velocity_avg, user_velocity_static,
       cast(publisher_views AS double)/publisher_impressions AS pav,
       cast(browsi_views AS double)/browsi_impressions AS bav,
       avg_user_latency, row_number() over (partition by user_id order by now desc) as rnk
FROM analytic
WHERE est_time>=date '2019-03-26'  AND device_type<>'DESKTOP' and site_key<>'ynetapp'
) AA

where rnk=1
) A


on S.user_id=A.user_id

order by rand()
limit 1000000